 # spine-flutter
 
flutter  可用的spineWidget,对应的spine 版本3.6.xx ~ 3.7.xx

调用方法:
pubsec.yaml 中引入:
  spine_flutter:
    git:
      url: git@gitee.com:PiaoMiaoDeYun/spine-flutter.git

dart中使用:

import 'package:spine_flutter/spine_flutter.dart';

SpineWidget(
	assets: Assets(
        skeltonDataFile: 'spineboy.json',
        atlasDataFile: 'spineboy.atlas',
        textureDataFile: 'spineboy.png',
        pathPrefix: 'assets/spineboy/',
        ),
    animationSettings: new AnimationSettings(0, 'walk', true),
    alignment: Alignment.center,
    fit: BoxFit.contain,
    //onCompleteCallback: ((trackEntry){
        //_animationSettings = new AnimationSettings(0, 'walk', true);
        //}),
    )
