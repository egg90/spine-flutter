library spine_flutter;

import 'package:flutter/material.dart' hide Texture;
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'spine_core/spine_core.dart' as core;

import 'dart:convert';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:async';
import 'dart:ui' as ui;

part 'asset_loader.dart';
part 'asset_manager.dart';
part 'texture.dart';
part 'animation_state.dart';
part 'skeleton_animation.dart';
part 'skeleton_render_object_widget.dart';
part 'skeleton_renderer.dart';


class SpineWidget extends LeafRenderObjectWidget {
  final Assets assets;
  final String skinName;
  final AnimationSettings animationSettings;
  final BoxFit fit;
  final Alignment alignment;
  final PlayState playState;
  final core.TrackEntryCallback onStartCallback;
  final core.TrackEntryCallback onInterruptCallback;
  final core.TrackEntryCallback onEndCallback;
  final core.TrackEntryCallback onDisposeCallback;
  final core.TrackEntryCallback onCompleteCallback;
  final core.TrackEntryEventCallback onEventCallback;

  const SpineWidget(
      {this.assets,
        this.skinName,
        this.animationSettings,
        this.fit,
        this.alignment = Alignment.center,
        this.playState = PlayState.Playing,
        this.onStartCallback,
        this.onInterruptCallback,
        this.onEndCallback,
        this.onDisposeCallback,
        this.onCompleteCallback,
        this.onEventCallback});

  @override
  RenderObject createRenderObject(BuildContext context) {
    debugPrint("called: createRenderObject");
    return new SkeletonRenderObject()
      ..assets = assets
      ..fit = fit
      ..alignment = alignment
      ..skinName = skinName
      ..animationSettings = animationSettings
      ..onStartCallback = onStartCallback
      ..onInterruptCallback = onInterruptCallback
      ..onEndCallback = onEndCallback
      ..onDisposeCallback = onDisposeCallback
      ..onCompleteCallback = onCompleteCallback
      ..onEventCallback = onEventCallback

      ..playState =
      (playState == PlayState.Playing && animationSettings != null)
          ? PlayState.Playing
          : PlayState.Paused;

  }

  @override
  void updateRenderObject(
      BuildContext context, covariant SkeletonRenderObject renderObject) {
    renderObject
      ..assets = assets
      ..fit = fit
      ..alignment = alignment
      ..skinName = skinName
      ..animationSettings = animationSettings
      ..playState =
      (playState == PlayState.Playing && animationSettings != null)
          ? PlayState.Playing
          : PlayState.Paused;
  }
}
